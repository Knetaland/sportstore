﻿
// set selected category and products under it
var setCategory = function (category) {
    // set the selected category
    customerModel.selectedCategory(category);
    filterProductByCategory();

    setView('list');
}

// set current view
var setView = function (view) {
    customerModel.currentView(view);
}


var addToCart = function (p) {
    var found = false;
    var items = customerModel.cartItems();
    for (var i = 0; i < items.length; i++) {
        // means item already in cart, just increment the count
        if (items[i].product.Id === p.Id) {
            found = true;
            count = items[i].count + 1;
            // remove current item from the cart
            customerModel.cartItems.splice(i, 1);
            customerModel.cartItems.push({
                count: count,
                product: p
            });
            break;
        }        
    }
    // if item not found, means it's new
    if (!found) {
        customerModel.cartItems.push({
            count: 1,
            product: p
        })
    }
    // set the view to show list
    setView('cart');
}


var removeFromCart = function (p) {
    customerModel.cartItems.remove(p);
}

var placeOrder = function () {
    var order = {
        Customer: model.username(),
        Lines: customerModel.cartItems().map(function (item) {
            return {
                Count: item.count,
                ProductId: item.product.Id
            }
        })
    };

    orderCtrl.saveOrder(order, function () {
        setView("thankyou");
    })
}

// add subscription to product
model.products.subscribe(function (newProduct) {
    // filter the products
    filterProductByCategory();

    customerModel.productCategories.removeAll();
    // map all the categories
    customerModel.productCategories.push.apply(customerModel.productCategories,
        model.products().map(function (p) {
            return p.Category;
        }).filter(function (value, index, self) {
            // self = the array itself
            // value = "item"
            // index = current index
            // method is getting the unique categories by using indexOf(item), this method will return the first
            // occurence of the value; so only if the item appears for the first time, it matches the index, or 
            // it will not match
            return self.indexOf(value) === index
        }).sort()
        );
});


// add subscription to 
customerModel.cartItems.subscribe(function (allItems) {
    // reduce is used to loop through array from left to right and reduce it to a single value
    // so here we have multiple items in the car
    customerModel.cartTotal(allItems.reduce(function (prev, item) {
        return prev + (item.count * item.product.Price);
    }, 0));

    customerModel.cartCount(allItems.reduce(function (prev, item) {
        return prev + item.count;
    },0))
})


var filterProductByCategory = function () {
    var selectedCategory = customerModel.selectedCategory();

    customerModel.filteredProducts.removeAll();
    customerModel.filteredProducts.push.apply(customerModel.filteredProducts,
        model.products().filter(function (p) {
            return selectedCategory == null || p.Category == selectedCategory;
        }));
}

$(document).ready(function () {
        productCtrl.getProducts();    
});
