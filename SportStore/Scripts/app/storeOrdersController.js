﻿var orderCtrl = (function () {
    var module = {};
    var ordersUrl = "/nonrest/orders";
    var ordersListUrl = ordersUrl + "/list";
    var ordersCreateUrl = ordersUrl + "/createorder/";
    var ordersDeleteUrl = ordersUrl + "/deleteorder/";

    module.getOrders = function () {
        sendRequest(ordersListUrl, "GET", null, function (data) {
            model.orders.removeAll();
            model.orders.push.apply(model.orders, data);
        });
    }

    module.saveOrder = function (order, successCallback) {
        sendRequest(ordersCreateUrl, "POST", order, function () {
            if (successCallback) {
                successCallback();
            }
        });
    }

    module.deleteOrder = function (id) {
        sendRequest(ordersDeleteUrl + id, "DELETE", null, function () {
            model.orders.remove(function (item) {
                return item.Id == id;
            })
        });
    }

    return module;
}());


