﻿var productCtrl = (function () {
    var module = {};
    var productUrl = "/api/products/";

    module.getProducts = function () {
        sendRequest(productUrl, "GET", null, function (data) {
            model.products.removeAll();
            model.products.push.apply(model.products, data);
        })
    };

    module.deleteProduct = function (id) {
        sendRequest(productUrl + id, "DELETE", null, function () {
            model.products.remove(function (item) {
                return item.Id == id;
            })
        });
    }

    module.saveProduct = function (product, successCallback) {
        sendRequest(productUrl, "POST", product, function () {
            getProducts();
            if (successCallback) {
                successCallback();
            }
        });
    }

    return module;

}());
