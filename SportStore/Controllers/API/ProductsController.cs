﻿using SportStore.Models;
using SportStore.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SportStore.Controllers.API
{
    public class ProductsController : ApiController
    {
        private IRepository repo;

        public ProductsController()
        {
            repo = (IRepository)GlobalConfiguration.Configuration.
                DependencyResolver.GetService(typeof(IRepository));

        }

        public IEnumerable<Product> GetProducts()
        {
            return repo.Products;
        }

        public IHttpActionResult GetProduct(int id)
        {
            var result = repo.Products.Where(p => p.Id == id).FirstOrDefault();
            if(result == null)
            {
                return BadRequest("No product Found");
            }
            return Ok<Product>(result);
        }

        public async Task PostProduct(Product product)
        {
            if (ModelState.IsValid)
            {
                await repo.SaveProductAsync(product);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }           
        }

        [Authorize(Roles ="Admin")]
        public async Task DeleteProduct(int id)
        {
            await repo.DeleteProductAsync(id);
        }

    }
}
