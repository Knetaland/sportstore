﻿using SportStore.Models;
using SportStore.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SportStore.Controllers
{
    public class OrdersController : ApiController
    {
        private IRepository repo;
        public OrdersController()
        {
            repo = (IRepository)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IRepository));
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IEnumerable<Order> List()
        {
            return repo.Orders;
        }

        public async Task<IHttpActionResult> CreateOrder(Order order)
        {
            if (ModelState.IsValid)
            {
                IDictionary<int, Product> products = repo.Products
                    .Where(p => order.Lines.Select(ol => ol.ProductId)
                        .Any(id => id == p.Id)).ToDictionary(p => p.Id);

                order.TotalCost = order.Lines.Sum(ol =>
                    ol.Count * products[ol.ProductId].Price);
                await repo.SaveOrderAsync(order);
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Admin")]
        public async Task DeleteOrder(int id)
        {
            await repo.DeleteOrderAsync(id);
        }
    }
}