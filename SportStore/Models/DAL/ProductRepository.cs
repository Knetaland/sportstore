﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SportStore.Models.DAL
{
    public class ProductRepository : IRepository
    {
        private ProductDbContext ctx = new ProductDbContext();

        public IEnumerable<Order> Orders
        {
            get
            {
                return ctx.Orders.Include("Lines").Include("Lines.Product");
            }
        }

        public IEnumerable<Product> Products
        {
            get
            {
                return ctx.Products;
            }
        }

        public async Task<Order> DeleteOrderAsync(int orderID)
        {
            Order o = ctx.Orders.Find(orderID);
            if(o != null)
            {
                ctx.Orders.Remove(o);
            }
            await ctx.SaveChangesAsync();
            return o;
        }

        public async Task<Product> DeleteProductAsync(int productID)
        {
            Product p = ctx.Products.Find(productID);
            if(p != null)
            {
                ctx.Products.Remove(p);
            }
            await ctx.SaveChangesAsync();
            return p;
        }

        public async Task<int> SaveOrderAsync(Order order)
        {
            if(order.Id == 0)
            {
                ctx.Orders.Add(order);
            }
            return await ctx.SaveChangesAsync();
        }

        public async Task<int> SaveProductAsync(Product product)
        {
            if(product.Id == 0)
            {
                ctx.Products.Add(product);
            }
            else
            {
                Product p = ctx.Products.Find(product.Id);
                if(p != null)
                {
                    p.Name = product.Name;
                    p.Description = product.Description;
                    p.Price = product.Price;
                    p.Category = product.Category;
                }
            }
            return await ctx.SaveChangesAsync();
        }
    }
}