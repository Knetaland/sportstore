﻿using PartyInvite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartyInvite.DAL
{
    public class Repo
    {
        private static Dictionary<string, GuestRsp> responses;

        static Repo()
        {
            responses = new Dictionary<string, GuestRsp>();
            responses.Add("Bob", new GuestRsp
            {
                Name = "Bob",
                Email = "bob@example.com",
                WillAttend = true
            });
            responses.Add("Alice", new GuestRsp
            {
                Name = "Alice",
                Email = "alice@example.com",
                WillAttend = true
            });
            responses.Add("Paul", new GuestRsp
            {
                Name = "Paul",
                Email = "paul@example.com",
                WillAttend = true
            });
        }

        public static void Add(GuestRsp newResponse)
        {
            string key = newResponse.Name.ToLowerInvariant();
            if (responses.ContainsKey(key))
            {
                responses[key] = newResponse;
            }
            else
            {
                responses.Add(key, newResponse);
            }
        }

        public static IEnumerable<GuestRsp> Responses
        {
            get { return responses.Values; }
        }

    }
}