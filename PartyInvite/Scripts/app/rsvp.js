﻿var model = {
    view: ko.observable('welcome'),
    rsvp: {
        name: ko.observable(''),
        email: '',
        willattend: ko.observable('')
    },
    attendees: ko.observableArray([])
};

var showForm = function () {
    model.view('form');
};

var sendRsvp = function () {
    $.post('/api/rsvp', {
        name: model.rsvp.name(),
        email: model.rsvp.email,
        willattend: model.rsvp.willattend()
    }).done(function (rsp) {
        getAttendees();
    }).fail(function (xhrs, status, error) {
        console.log(error);
    });
};

var getAttendees = function () {
    $.get('/api/rsvp')
        .done(function (data) {
            model.attendees.removeAll();
            model.attendees.push.apply(model.attendees, data.map(function (rsvp) {
                return rsvp.Name;
            }));
            model.view('thanks');
        });
};

$(document).ready(function () {
    ko.applyBindings();
});


