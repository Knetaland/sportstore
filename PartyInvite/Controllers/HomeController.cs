﻿using PartyInvite.DAL;
using PartyInvite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PartyInvite.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Rsvp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Rsvp(GuestRsp response)
        {
            if (ModelState.IsValid)
            {
                Repo.Add(response);
                return View("Thanks", response);
            }
            else
            {
                return View();
            }
        }

        [ChildActionOnly]
        public ActionResult Attendees()
        {
            return View(Repo.Responses.Where(x => x.WillAttend == true));
        }

    }
}