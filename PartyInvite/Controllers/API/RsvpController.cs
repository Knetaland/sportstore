﻿using PartyInvite.DAL;
using PartyInvite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PartyInvite.Controllers.API
{
    public class RsvpController : ApiController
    {


        public IHttpActionResult GetAttendees()
        {
            return Ok<IEnumerable<GuestRsp>>(Repo.Responses.Where(x => x.WillAttend == true));
        }


        public IHttpActionResult PostResponse(GuestRsp response)
        {
            if (ModelState.IsValid)
            {
                Repo.Add(response);
                return Ok();
            }
            return BadRequest();
        }

    }
}
