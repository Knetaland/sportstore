﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace PartyInvite.Controllers.API
{
    public class PageSizeController : ApiController
    {
        private static string TargetUrl = "http://apress.com";
        private Logger log = LogManager.GetCurrentClassLogger();

        public async Task<long> GetPageSize(CancellationToken cToken)
        {
            using (var wc = new WebClient())
            {
                Stopwatch sw = Stopwatch.StartNew();
                var data = await wc.DownloadDataTaskAsync(TargetUrl);
                log.Info("Elapsed time: {0}ms", sw.ElapsedMilliseconds);
                return data.LongLength;
            }
        }
    }
}
